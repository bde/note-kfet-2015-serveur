#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Ancien listing.py
# Adapté à la NK2015 par Skippy
#

""" Destiné à envoyé un mail récapitulatif des consos de la
    semaine aux gens intéressés """

import sys
import mail
import argparse

sys.path.append("../config")
import config

debug = False

def _ramassage(cur, out, idbde=0, delai=7, date1=None, date2=None):
    
    case = lambda s: u"""CASE
               WHEN %s =  0 THEN 'Bde'
               WHEN %s = -1 THEN 'Chèques'
               WHEN %s = -2 THEN 'Espèces'
               WHEN %s = -3 THEN 'Virements'
               WHEN %s = -4 THEN 'CB'
               ELSE 'PROBLEM !' END""" % (s, s, s, s, s)

    if not date1:
        date = delai
        delaioudate = "AND (CURRENT_TIMESTAMP-date) <= '%s days'"
    else:
        date = date1
        delaioudate = "AND date > %s"
        if date2:
            delaioudate += " AND date < %s"

    requests = {
        "boutons" : { "sfield" : u"description",
                      "join"   : u"",
                      "where"  : u"AND %s = '%%s' AND type = 'bouton' " % ( u"emetteur" if out else u"destinataire" ) },
        "retraits": { "sfield" : case(u"destinataire") if out else case(u"emetteur"),
                      "join"   : u"",
                      "where"  : u"""AND ('0' = '%%s' OR %s = '%%s')
                                     AND type = '%s'""" % ( u"emetteur" if out else u"destinataire",
                                                            u"retrait" if out else u"crédit" ) },
        "transferts" : { "sfield" : u"pseudo",
                         "join"   : u"INNER JOIN comptes ON %s = idbde " % ( u"destinataire" if out else u"emetteur" ),
                         "where"  : u"AND %s = '%%s' AND transactions.type = 'transfert'" % ( u"emetteur" if out else u"destinataire" ) },
        "don" : { "sfield" : u"pseudo",
                         "join"   : u"INNER JOIN comptes ON %s = idbde " % ( u"destinataire" if out else u"emetteur" ),
                         "where"  : u"AND %s = '%%s' AND transactions.type = 'don'" % ( u"emetteur" if out else u"destinataire" ) },
        "adhesion": { "sfield"    : u"description",
                      "join"      : u"",
                      "where"     : u"AND emetteur = '%s' AND transactions.type = 'adhésion'" }
    }
    requests_template = """ SELECT %s AS field, SUM(quantite) AS qte, SUM(quantite*montant) AS mt
                            FROM transactions %s
                            WHERE valide
                              AND NOT emetteur = destinataire
                              %s
                              %s
                            GROUP BY field
                            ORDER BY mt DESC; """ # % ( sfield, join, where, delaioudate)
    res = {}
    for mode in requests:
        # On construit la requête
        if mode == "adhesion" and not out:
            continue
        struct = requests[mode]
        req = requests_template % ( struct["sfield"], struct["join"], struct["where"], delaioudate)
        
        # On construit les arguments
        reqargs = (idbde, date)
        if mode == "retraits":
            reqargs = (idbde,) + reqargs
        if date1 and date2:
            reqargs += (date2,)

        # On exécute le tout
        if debug:
            print mode.encode('utf-8'), req.encode('utf-8'), reqargs
        cur.execute(req, reqargs)
        res[mode] = cur.fetchall()
        
    return res

def ramassage(cur, idbde=0, delai=7, date1=None, date2=None):
    return _ramassage(cur, True, idbde, delai, date1, date2), _ramassage(cur, False, idbde, delai, date1, date2)
        
def sqlresult_nice(l):
    if not l:
        return l
    # On decode les str de la base
    for result in l:
        for (k, v) in result.items():
            if (type(v) == str):
                result[k] = v.decode("utf-8")
    l = [dict(i) for i in l]
    return l

def sqlresult_pprint(l, columns, column_names, columns_preprocess=None):
    """Pretty print de la liste"""
    if not l:
        return u""
        
    out = u""
    
    # On preprocess les colonnes
    for result in l:
        for (column, column_function) in columns_preprocess.iteritems():
            result[column] = unicode(column_function(result[column]))
            # Je suis pas sûr que ce soit le plus propre... #SkippyLambdaHack
    
    # On évalue quelles sont les tailles des colonnes nécessaires
    maxs = {col : max(len(column_names[col]), max([len(resultat[col]) for resultat in l])) for col in columns}
    
    # On affiche
    heads = ["%s%s%s" % (u" " * ((maxs[col] - len(column_names[col]))/2),
                         column_names[col],
                         u" " * ((maxs[col] - len(column_names[col]) + 1)/2))
             for col in columns]
    title = u" " + u" | ".join(heads)
    # Le +2 correspond aux espaces de début et de fin de colonne
    separator = u"+".join([u"-" * (maxs[col] + 2) for col in columns])
    out += title + u"\n" + separator + u"\n"
    
    for fields in l:#for nom, pnom, pseudo, mail, solde, duree in l:
        line = u" " + u" | ".join([fields[col] + u" " * (maxs[col] - len(fields[col])) for col in columns]) + u" "
        out += line + "\n"
    return out

def _rapport(out, sqlresult, titles, labels, club):
    res = u""
    aggr = 0.
    montant_tot = lambda r: sum([ float(l["mt"]) for l in r ])

    for mode in [u"boutons", u"retraits", u"transferts", u"don", u"adhesion"]:
        # Il n'y a pas de recettes via boutons pour les non-clubs
        if not club and mode == u"boutons" and not out:
            continue
        if mode == u"adhesion" and (mode not in sqlresult or not sqlresult[mode]):
            continue
        res += u"=== %s === " % (titles[mode],)
        if mode in sqlresult and sqlresult[mode]:
            nice = sqlresult_nice(sqlresult[mode])
            res += u"\n"
            res += sqlresult_pprint(nice, [ u"field", u"qte", u"mt" ],
                                    { u"field":labels[mode], u"qte":u"Quantité", u"mt":u"Montant" },
                                    { u"qte":lambda s: s, u"mt": lambda s: s / 100. })
            mt   = montant_tot(nice)
            res += u"Montant total: %s€\n\n" % (mt,)
            aggr+= mt
        else:
            res += u"Aucun%ss\n" % (u"e" if (out and mode == u"boutons") else u"",)
    return res, aggr

def rapport(depenses, apports, compte, delai = None, date1 = None, date2 = None):
    """Génère un rapport"""

    res = u"Rapport d'activité de %s %s (note: %s)"%(compte["prenom"], compte["nom"], compte["pseudo"])
    if delai:
        res += u" depuis %s jours" % (unicode(delai))
    elif date1:
        res += u"\n\tDepuis le %s" % (unicode(date1))
        if date2:
            res += u"\n\tJusqu'au %s" % (unicode(date2))
    res+= u"\n\n"
    res+= u"= TL;DR = \nDépenses totales: %s€ \nApports totaux: %s€ \nDifférentiel: %s€ \nNouveau solde: %s€\n\n"
    res+= u"= Rapport détaillé = \n"

    res+= u"== Dépenses == \n\n"
    
    titles = { u"boutons":u"Consommations via boutons",
               u"retraits":u"Retraits",
               u"transferts":u"Transferts",
               u"don":u"Dons émis",
               u"adhesion":u"Adhésions"}
    labels = { u"boutons":u"Description",
               u"retraits":u"Type",
               u"transferts":u"Destinataire",
               u"don":u"Destinataire",
               u"adhesion":u"Description"}
    txt, depenses_tot = _rapport(True, depenses, titles, labels, compte[u"type"] == u"club")
    res += txt

    res+= u"== Apports == \n\n"
    titles = { u"boutons":u"Revenus via boutons",
               u"retraits":u"Crédits",
               u"transferts":u"Transferts",
               u"don":u"Dons reçus"}
    labels[u"transferts"] = u"Émetteur"
    labels[u"don"] = u"Émetteur"
    txt, apports_tot = _rapport(False, apports, titles, labels, compte[u"type"] == u"club")
    res += txt

    res += "\n-- \nLes rapports de la NK2015 \n#stvcqjvd\n"
    diff = apports_tot - depenses_tot
    return res % (depenses_tot, apports_tot, diff, float(compte["solde"]) / 100.0)

def ramassage_comptes(cur):
    # Ici on veut ramasser tous les comptes tels que:
    # date_du_dernier_rapport + fréquence < NOW()
    # Et il existe une transaction depuis le dernier rapport
    req = """SELECT idbde, nom, prenom, pseudo, mail, type, previous_report_date, solde
             FROM comptes
             WHERE not deleted
               AND report_period > 0
               AND date_trunc('day', previous_report_date + interval '1' day * report_period) < NOW()
               AND EXISTS (SELECT * FROM transactions
                           WHERE valide
                             AND (emetteur = idbde OR destinataire = idbde)
                             AND date > previous_report_date);"""
    cur.execute(req)
    comptes = cur.fetchall()
    if debug:
        print comptes
    return sqlresult_nice(comptes)

def envoyer_rapports(cur, comptes):

    disclaimer = u"""Bonjour,

Vous recevez ce mail car vous avez défini une "Fréquence des rapports" dans la Note.
Ce service vient d'entrer en fonctionnement et n'est encore qu'en béta.
Le premier rapport récapitule toutes vos consommations depuis le début de la Note Kfet 2015.
Ensuite, un rapport vous est envoyé à la fréquence demandée seulement si vous avez consommé
depuis le dernier rapport.
Pour arrêter de recevoir des rapports, il vous suffit de modifier votre profil Note et de
mettre la fréquence des rapports à 0 ou -1.
Pour toutes suggestions par rapport à ce service, contactez respo-info.bde@lists.crans.org

"""

    for c in comptes:
        cur.execute("""UPDATE comptes SET previous_report_date=NOW() WHERE idbde=%s; COMMIT;
                       SELECT previous_report_date FROM comptes WHERE idbde=%s;""", (c["idbde"],c["idbde"]))
        now = cur.fetchone()["previous_report_date"]
        dep, app = ramassage(cur, c["idbde"], delai=None, date1=c["previous_report_date"], date2=now)
        rap = rapport(dep, app, c, delai=None, date1=c["previous_report_date"], date2=now)
        if c["previous_report_date"].year == 2014:
            rap = disclaimer+rap
        if debug:
            print c, now, rap.encode('utf-8')
        else:
            mail.queue_mail(config.mails_from, [c["mail"]], u"[Note Kfet] Rapport de la Note Kfet", rap)

if __name__=="__main__":
    con, cur = mail.getcursor(config.database)
    #debug = True
    comptes = ramassage_comptes(cur)
    envoyer_rapports(cur, comptes)
