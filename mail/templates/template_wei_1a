English version below

Bonjour {{ user.prenom.title() }} {{ user.nom.title() }}

Félicitations! Ton inscription au "{{ wei.wei_name.title() }}" est désormais enregistrée, ainsi que ton adhésion au BdE.

---- Concernant l'adhésion ----

Ton adhésion est incluse lorsque tu viens au WEI. Elle te permet d'accéder à tous les services proposés par le BdE, notamment l'accès à la Kfet, aux [Pot]s, et tu pourras ainsi venir manger à la Kfet le midi en semaine et pour les petits-déjeuners du week-end, ainsi que consommer au bar tous les jours.
En même temps que l'adhésion, tu es inscrit sur la note, un porte-monnaie virtuel qu'il te faudra remplir et qui te servira à régler tes consommations à la Kfet toute l'année. Tu y seras enregistré dans très peu de temps, et tu recevras un autre mail te confirmant la création de ta note.

---- Concernant le WEI ----

{% if user.soge -%}
Tu as choisi d'ouvrir un compte à la Société Générale et nous t'en remercions! N'oublie pas de finir toutes les formalités avec la Société Générale en venant rencontrer ses agents tous les midis et tous les soirs à la Kfet.
{%- else -%}
Tu as choisi de ne pas ouvrir un compte à la Société Générale. Sache que tu peux toujours le faire en venant rencontrer les agents de la Société Générale les midis et les soirs tous les jours de la semaine (week-end exclu). Si tu choisis de ne pas ouvrir de compte, il te faudra payer de ta poche en venant remplir ta note à la Kfet de {% if user.normalien %}{{wei.prix_wei_normalien }}{% else %}{{ wei.prix_wei_non_normalien }}{% endif %}€ (par carte bleue, chèque ou espèces) avant le mercredi {{wei.wei_begin.day - 2 }} septembre.
{% endif -%}

Le WEI aura lieu du vendredi {{ wei.wei_begin.day }} au dimanche {{ wei.wei_end.day }} septembre. Le vendredi les cours prendront fin à 12h, heure à laquelle nous t'attendrons devant le bâtiment Léonard de Vinci. Il y aura à manger et à boire sur place, auprès de ton équipe et de ton bus que tu découvriras à ce moment-là, juste avant le départ. Nous nous engageons à ce qu'il n'y ait aucun bizutage pendant ce week-end que nous te promettons d'être inoubliable: ce week-end d'intégration est le tien et le BdE va te faire vivre 2 jours d'intense folie ! L'heure de retour sur le campus le dimanche est estimée à 22h et une Kokarde t'attendra à la descente du bus pour finir ce WEI en apothéose !

Pour que tout se passe pour le mieux, nous te demandons d'apporter un sac (de préférence peu volumineux) contenant:
    *   un duvet,
    *   des vêtements risk-free,
    *   des vêtements chauds,
    *   des vêtements de rechange,
    *   un maillot de bain,
    *   une serviette,
    *   une pièce d'identité.

N'apporte rien de précieux. Pas besoin d'argent ni de portefeuille. En bref : aucune tenue correcte exigée.

Nous te souhaitons une excellente rentrée!

À très bientôt!

-- 
Les Grands Chefs WEI


===============================================================================


Useful acronyms
- WEI (Week-end d'Intégration) : Orientation weekend
- BdE (Bureau des Élèves) : Student council - the BdE is actually a student society which you can join to enjoy full access to the services provided by the council representatives

Hi {{ user.prenom.title() }} {{ user.nom.title() }}

Congratulations! Your registration to the "{{ wei.wei_name.title() }}" (Orientation Weekend) has been successfully processed, and you are now a registered member of the BdE as well.

---- BdE Membership ----

When you choose to attend Orientation weekend, you gain automatic BdE membership. BdE membership allows you to enjoy all the services offered by the BdE, such as/including access to the student-managed cafeteria and lounge (Kfet) and to the weekly student parties on Tuesday evenings (Pots) - free food, free beer and up to 4 free cocktails! BdE membership also means you can have lunch at the Kfet during the week, breakfast on the weekends, and buy drinks at the bar everyday. You will soon possess
a 'note' account - a virtual wallet on which you can deposit money to pay for your meals and drinks. We will send you another confirmation email very shortly, as soon as your account is created.

---- For the WEI (Orientation weekend) ----

{% if user.soge -%}
You chose to open a bank account at Société Générale and we thank you for it! Don't forget to complete all the paperwork with Société Générale by meeting with its employees at the Kfet at lunchtime or in the evening, on week days (weekend excluded).
{%- else -%}
You did not choose to open a bank account at Société Générale. Remember you can still do so by meeting with its employees at lunchtime and in the evening on week days (weekend excluded). If you choose not to open an account, you will have to pay your attendance to Orientation weekend yourself by depositing €{% if user.normalien %}{{wei.prix_wei_normalien }}{% else %}{{ wei.prix_wei_non_normalien }}{% endif %} on your "Note Kfet" (via credit card, chack or in cash), before Wednesday, September {{ wei.wei_begin.day - 2 }}.
{% endif -%}

Orientation weekend will take place from Friday, September {{ wei.wei_begin.day }} to Sunday, September {{ wei.wei_end.day }}. On Friday {{ wei.wei_begin.day }}, classes will end at 12 am, at which time we will be waiting for you outside the Léonard de Vinci building. You will find food and drinks, along with your team and bus, which you will both discover before leaving. We are committed to providing you with a 100% hazing-free experience, to ensure you spend an unforgettable weekend ! This orientation weekend is fully yours and we promise you will spend 2 crazy, crazy days under your orientation leaders' guidance!

Orientation weekend is estimated to end with the return trip to campus on Sunday evening, around 10pm. A party will await you then so that the weekend truly ends with a bang!

To ensure everything runs smoothly, we ask you to bring a bag (if possible not too bulky) containing : 
    *   a sleeping bag,
    *   old clothes you don't mind ruining,
    *   warm clothes,
    *   a change of clothes,
    *   a swimsuit,
    *   a towel,
    *   an ID Card.

Don't bring anything valuable! No need for money or even a wallet. The only valuable you should bring is your ID. In short, there is no dress code!

We wish you an excellent time discovering campus and meeting other students!

See you (very) soon!

-- 
The WEI Team
